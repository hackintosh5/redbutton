/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

buildscript {
    repositories {
        jcenter()
    }
}

plugins {
    kotlin("multiplatform") version "1.3.71"
    kotlin("plugin.serialization") version "1.3.71"
}
repositories {
    jcenter()
    maven("https://dl.bintray.com/kotlin/ktor")
    maven("https://jitpack.io")
    mavenCentral()
}
val ktorVersion = "1.3.2"
val logbackVersion = "1.2.3"
val ktorRateLimitingVersion = "e133be9"
val serializationVersion = "0.20.0"
val coroutinesVersion = "1.3.5"
val htmlVersion = "0.7.1"

kotlin {
    jvm()
    js {
        browser {
        }
    }
    sourceSets {
        jvm().compilations["main"].defaultSourceSet {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("io.ktor:ktor-server-netty:$ktorVersion")
                implementation("io.ktor:ktor-websockets:$ktorVersion")
                implementation("io.ktor:ktor-html-builder:$ktorVersion")
                implementation("ch.qos.logback:logback-classic:$logbackVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
            }
        }
        js().compilations["main"].defaultSourceSet {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-js:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:$coroutinesVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-html-js:$htmlVersion")
            }
        }
    }
}

tasks {
    val jsBrowserProductionWebpack = getByName<org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpack>("jsBrowserProductionWebpack")
    jsBrowserProductionWebpack.destinationDirectory = kotlin.sourceSets["jvmMain"].resources.srcDirs.first()
    val classpath = kotlin.jvm().compilations["main"].runtimeDependencyFiles + getByName("jvmJar").outputs.files

    fun baseJar(task: Jar) = task.run {
        dependsOn(jsBrowserProductionWebpack)
        from(jsBrowserProductionWebpack.destinationDirectory)
        manifest {
            attributes("Main-Class" to "tk.hack5.redbutton.server.MainKt")
        }
    }

    getByName<Jar>("jvmJar") { baseJar(this) }

    task<JavaExec>("run") {
        dependsOn(getByName("jvmJar"))
        group = "application"
        main = "tk.hack5.redbutton.server.MainKt"
        classpath(classpath)
    }

    task<Jar>("fatJar") {
        baseJar(this)
        dependsOn(getByName("jvmJar"))
        from({ classpath.filter { it.name.endsWith("jar") }.map { zipTree(it) } })
    }

    task("stage") {
        mustRunAfter(clean)
        dependsOn(getByName("fatJar"))
    }
}