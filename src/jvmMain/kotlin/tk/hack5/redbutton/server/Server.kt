/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.server

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ConditionalHeaders
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.resource
import io.ktor.http.content.static
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.html.*

fun serve(production: Boolean = true) {
    val rooms = Rooms()
    embeddedServer(Netty, port = System.getenv("PORT")?.toIntOrNull() ?: 8080, host = "0.0.0.0") {
        install(WebSockets) {
            pingPeriodMillis = 10000
            timeoutMillis = 20000
            maxFrameSize = 128
        }
        install(ConditionalHeaders)
        routing {
            get("/") {
                call.respondHtml {
                    head {
                        title("Button")
                        link(href = "https://fonts.googleapis.com/css2?family=Rubik:wght@300&display=swap", rel = "stylesheet")
                        meta {
                            name = "viewport"
                            content = "user-scalable=no" // https://stackoverflow.com/a/24172998/5509575
                        }
                    }
                    body {
                        div(classes = "fill-screen loading-background fade") {
                            id = "loading-container"
                            div(classes = "center") {
                                h1 {
                                    id = "loading-title"
                                    +"Loading Button..."
                                }
                                h6 {
                                    id = "loading-tip"
                                }
                            }
                        }
                        style {
                            unsafe {
                                +loadingStyles
                            }
                        }
                        style {
                            unsafe {
                                +styles
                            }
                        }
                        script(src = "/static/redbutton.js") {
                            async = true
                        }
                        link(href = "https://fonts.googleapis.com/icon?family=Material+Icons", rel = "stylesheet")
                        div(classes = "fill-screen room-background fade reverse") {
                            id = "room-container"
                            div(classes = "center") {
                                input(type = InputType.text, classes = "input autofocus") {
                                    id = "room-input"
                                    autoFocus = true
                                    placeholder = "Room name..."
                                    size = "15"
                                    maxLength = "15"
                                }
                                br {}
                                br {}
                                input(type = InputType.button, classes = "input") {
                                    id = "room-done"
                                    value = "Done"
                                }
                            }
                            div(classes = "footer") {
                                span(classes = "footer-left") {
                                    +"Copyright © 2020 Hackintosh5"
                                }
                                a(href = "https://gitlab.com/hackintosh5/redbutton", target = "_blank", classes = "footer-right") {
                                    +"Source Code"
                                }
                            }
                        }
                        div(classes = "fill-screen name-background fade reverse") {
                            id = "name-container"
                            div(classes = "center") {
                                input(type = InputType.text, classes = "input autofocus") {
                                    id = "name-input"
                                    autoFocus = true
                                    placeholder = "Your name..."
                                }
                                br {}
                                br {}
                                input(type = InputType.button, classes = "input") {
                                    id = "name-done"
                                    value = "Done"
                                }
                            }
                        }
                        div(classes = "fill-screen content-background fade reverse") {
                            id = "button-container"
                            span(classes = "material-icons top-right clickable") {
                                id = "chat-button-button"
                                +"chat"
                            }
                            span(classes = "center circle drop-shadow autofocus") {
                                id = "button"
                            }
                            div(classes = "messages-box button-messages-box") {
                                id = "button-messages-box"
                            }
                            div(classes = "messages-box button-messages-box button-messages-box-overlay") {
                                // hack to achieve messages fading out
                            }
                        }
                        div(classes = "fill-screen content-background fade reverse") {
                            id = "chat-container"
                            span(classes = "material-icons top-right clickable") {
                                id = "chat-chat-button"
                                +"mark_chat_read"
                            }
                            div(classes = "messages-box chat-messages-box") {
                                id = "chat-messages-box"
                                form {
                                    id = "chat-message-form"
                                    input(type = InputType.text, classes = "input message-input autofocus") {
                                        id = "message-input"
                                        autoFocus = true
                                        placeholder = "Type your message..."
                                    }
                                    input(type = InputType.submit, classes = "invisible")
                                }
                            }
                        }
                        div(classes = "fill-screen won-background fade reverse") {
                            id = "won-container"
                            div (classes = "center") {
                                h1 {
                                    +"Speak now"
                                }
                                h5 {
                                    +"Click anywhere to dismiss"
                                }
                            }
                        }
                        div (classes = "fill-screen lost-background fade reverse") {
                            id = "lost-container"
                            h1(classes = "center") {
                                id = "lost-text"
                            }
                        }
                    }
                }
            }
            static("/static") {
                resource("redbutton.js")
                if (!production) {
                    resource("redbutton.js.map")
                }
            }
            webSocket(path = "/ws/{roomId}", handler = { handleWebSocket(this, rooms) })
            get("/ping") {
                call.respondText("", status = HttpStatusCode.OK)
            }
        }
    }.start(true)
}