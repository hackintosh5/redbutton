/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.server

import io.ktor.websocket.DefaultWebSocketServerSession
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class Rooms {
    private val data: MutableMap<String, MutableRoom> = mutableMapOf()
    private val lock = Mutex()

    suspend fun getRoom(id: String): Room = lock.withLock {
        data.getOrPut(id, { MutableRoom(mutableListOf()) })
    }
    suspend fun addSession(id: String, user: User): Room = lock.withLock {
        data.getOrPut(id, { MutableRoom(mutableListOf()) }).also {
            it.users.add(user)
        }
    }
    suspend fun removeSession(id: String, session: User) = lock.withLock {
        data[id]?.users?.remove(session) ?: false
    }
}

data class User(val session: DefaultWebSocketServerSession, val name: String, var stt: Int)

abstract class Room {
    abstract val users: List<User>
    val winLock = Mutex()
}
private class MutableRoom(override val users: MutableList<User>) : Room()