/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.server

import io.ktor.http.cio.websocket.*
import io.ktor.websocket.DefaultWebSocketServerSession
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.run

suspend fun handleWebSocket(session: DefaultWebSocketServerSession, rooms: Rooms) = session.run {
    val roomId = requireNotNull(call.parameters["roomId"]) { "roomId == null" }.toLowerCase()
    var user: User? = null
    var job: Job? = null
    var holdingLock = false
    var autoRelease: Job? = null

    try {
        val pendingPings = mutableMapOf<Byte, Long>()
        loop@for (frame in incoming) {
            when (frame) {
                is Frame.Binary -> {
                    val ts = System.nanoTime()
                    when(frame.buffer[0]) {
                         (-1).toByte() -> {
                             val room = rooms.getRoom(roomId)
                             val maxDelay = room.users.map(User::stt).max()
                             val thisDelay = (maxDelay!! - user!!.stt) / 1000000L
                             launch {
                                 delay(thisDelay)
                                 if (!room.winLock.tryLock()) return@launch
                                 holdingLock = true
                                 autoRelease?.cancel()
                                 autoRelease = launch {
                                     delay(10000)
                                     autoRelease = null
                                     if (holdingLock) {
                                         room.users.forEach {
                                             it.session.send("d")
                                         }
                                         holdingLock = false
                                         room.winLock.unlock()
                                     }
                                 }
                                 try {
                                     val users = room.users.filterNot { it == user!! }
                                     users.forEach {
                                         it.session.send("l" + user!!.name)
                                     }
                                     session.send("w")
                                 } catch (e: Throwable) {
                                     holdingLock = false
                                     room.winLock.unlock()
                                     throw e
                                 }
                             }
                         }
                         (-2).toByte() -> {
                             val room = rooms.getRoom(roomId)
                             room.users.filterNot { it == user }.forEach {
                                 it.session.send("d")
                             }
                             holdingLock = false
                             room.winLock.unlock()
                         }
                         else -> {
                             val key = frame.buffer[0]
                             user!!.stt = ((ts - pendingPings[key]!!) / 2).toInt()
                             pendingPings.remove(key)
                         }
                    }
                }
                is Frame.Close -> {
                    break@loop
                }
                is Frame.Ping -> {
                    outgoing.send(Frame.Pong(frame.buffer))
                }
                is Frame.Pong -> {
                }
                is Frame.Text -> {
                    if (user == null) {
                        user = User(session, frame.readText(), 0)
                        job = launch {
                            while (true) {
                                val key = (pendingPings.keys.min()?.plus(1) ?: 0).toByte()
                                pendingPings[key] = System.nanoTime()
                                session.send(byteArrayOf(key))
                                delay(1000)
                            }
                        }
                        rooms.addSession(roomId, user)
                    } else {
                        val room = rooms.getRoom(roomId)
                        val text = frame.readText()
                        val maxDelay = room.users.map(User::stt).max()
                        room.users.forEach {
                            launch {
                                val thisDelay = (maxDelay!! - user.stt) / 1000000L
                                delay(thisDelay)
                                it.session.send("m${user.name}: $text")
                            }
                        }

                    }
                }
            }
        }
    } catch (e: Throwable) {
        job?.cancelAndJoin()
        autoRelease?.cancelAndJoin()
        if (holdingLock) rooms.getRoom(roomId).winLock.unlock()
        user?.let { rooms.removeSession(roomId, it) }
        close(CloseReason(CloseReason.Codes.INTERNAL_ERROR, e.toString()))
    }
    job?.cancelAndJoin()
    autoRelease?.cancelAndJoin()
    if (holdingLock) rooms.getRoom(roomId).winLock.unlock()
    user?.let { rooms.removeSession(roomId, it) }
}