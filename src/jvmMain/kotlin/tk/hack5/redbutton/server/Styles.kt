/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.server

val loadingStyles = """
body {
    margin: 0px;
}
.fill-screen {
    min-width: 100%;
    min-height: 100%;
    position: fixed;
    overflow: auto;
}
.loading-background {
    background-color: #039be5;
}
.center {
    text-align: center;
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
h1 {
    font-size: 6rem;
}
h2 {
    font-size: 5rem;
}
h3 {
    font-size: 4rem;
}
h4 {
    font-size: 3rem;
}
h5 {
    font-size: 2rem;
}
h6 {
    font-size: 1rem;
}
body {
    color: #ffffff;
}
h1, h2, h3, h4, h5, h6 {
    font-family: Rubik, sans-serif;
}
"""//.replace("\n", "").replace(" ", "")
val styles = """
p {
    font-family: Arial, Helvetica, sans-serif;
}
.name-background {
    background-color: #0091ea;
}
.input {
    font-size: 2rem;
    color: #ffffff;
    background-color: #00000022;
    border: none;
    border-radius: 0.5rem;
    min-width: 5vw;
    max-width: 90vw;
    outline: none;
}
.footer {
    position: absolute;
    width: 100%;
    left: 50%;
    transform: translate(-50%, 0);
    bottom: 1rem;
}
.footer-left, .footer-right {
    font-family: Rubik, sans-serif;
    color: inherit;
}
.footer-left {
    padding-left: 1rem;
}
.footer-right {
    position: absolute;
    right: 0;
    text-align: right;
    padding-right: 1rem;
}
.room-background {
    background-color: #01579b;
}
.content-background {
    background-color: #03a9f4;
}
.won-background {
    background-color: #00b0ff;
}
.lost-background {
    background-color: #d32f2f;
}
@keyframes fade {
    from {
        opacity: 1;
        z-index: 1;
    }
    to {
        opacity: 0;
        z-index: 0;
    }
}
@keyframes fade-reverse {
    from {
        opacity: 0;
        z-index: 0;
    }
    to {
        opacity: 1;
        z-index: 1;
    }
}
.fade {
    animation-name: fade;
    animation-duration: 0.5s;
    animation-fill-mode: both;
    animation-play-state: paused;
}
.reverse {
    animation-name: fade-reverse;
}
.progress-bar-outer {
    border-radius: 0.5rem;
    padding: 0.5rem;
    height: 1rem;
    background-color: #29b6f6;
}
.progress-bar {
    width: 0%;
    transition: width 0.1s ease-in-out;
    height: 100%;
    border-radius: 0.5rem;
    background-color: #81d4fa;
}
.absolute {
    position: absolute;
}
.piece-border {
    border-style: solid;
}
.circle {
    width: 50vmin;
    height: 50vmin;
    top: 5vmin;
    transform: translate(-50%, 0);
    border-radius: 50%;
    background-color: #c62828;
}
.circle:active {
    background-color: #b71c1c;
}
.drop-shadow {
    box-shadow: 1vmin 2vmin #00000022;
}
.top-right {
    position: absolute;
    top: 3rem;
    right: 3rem;
}
.clickable {
    cursor: pointer;
}
.message-input {
    width: 100%
}
.messages-box {
    position: absolute;
    bottom: 0;
    width: 90%;
    left: 50%;
    transform: translate(-50%, 0);
    display: flex;
    flex-direction: column;
}
.chat-messages-box {
    bottom: 3rem;
}
.button-messages-box {
    bottom: 5vmin;
    height: 35vmin;
    overflow: hidden;
    flex-direction: column-reverse;
}
.button-messages-box-overlay {
    background-image: linear-gradient(#03a9f4ff, #03a9f400); // #03a9f4 
}
.chat-message {
    text-align: center;
    line-height: 0;
}
.invisible {
    visibility: hidden;
    position: absolute;
}
"""//.replace("\n", "").replace(" ", "")