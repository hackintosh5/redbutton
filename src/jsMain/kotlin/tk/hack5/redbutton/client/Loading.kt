/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.client

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.promise
import org.w3c.dom.*
import kotlin.browser.document
import kotlin.browser.window

fun main() {
    window.onerror = { _, _, _, _, error ->
        showError(error!!)
    }
    if (document.readyState != DocumentReadyState.LOADING) {
        init()
    } else {
        window.addEventListener("DOMContentLoaded", {
            init()
        })
    }
}

fun init() {
    GlobalScope.promise {
        initClicks()
        connectToServer()
        switchPage("name-container")
    }.catch {
        showError(it)
    }
}

fun nameDone() {
    send((document.getElementById("name-input") as HTMLInputElement).value)
    switchPage("button-container")
    gameVisible = true
}

fun roomDone() {
    window.location.hash = (document.getElementById("room-input") as HTMLInputElement).value
    if (gameId.isNotBlank())
        roomDoneWait.complete()
}
val roomDoneWait = Job()
fun promptGameId() {
    switchPage("room-container")
}

fun switchPage(toId: String) {
    if (toId == currentPage)
        return
    val from = (window.document.getElementById(currentPage) as HTMLDivElement)
    from.classList.remove("reverse")
    from.style.animationPlayState = "running"
    val to = (window.document.getElementById(toId) as HTMLDivElement)
    (to.getElementsByClassName("autofocus")[0] as? HTMLElement)?.focus()
    to.classList.add("reverse")
    to.style.animationPlayState = "running"
    currentPage = toId
}

var currentPage = "loading-container"