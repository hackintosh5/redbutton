/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.client

import org.w3c.dom.*
import org.w3c.dom.events.KeyboardEvent
import kotlin.browser.document

fun initClicks() {
    (document.getElementById("name-done") as HTMLInputElement).onclick = { nameDone() }
    (document.getElementById("name-input") as HTMLInputElement).onkeyup = { nameKeyUp(it) }
    (document.getElementById("room-done") as HTMLInputElement).onclick = { roomDone() }
    (document.getElementById("room-input") as HTMLInputElement).onkeyup = { roomKeyUp(it) }
    (document.getElementById("chat-button-button") as HTMLSpanElement).onclick = { enableChat() }
    (document.getElementById("chat-chat-button") as HTMLSpanElement).onclick = { disableChat() }
    (document.getElementById("chat-message-form") as HTMLFormElement).onsubmit = { sendChat(); it.preventDefault() }
    //(document.getElementById("message-input") as HTMLInputElement).onkeyup = { chatKeyUp(it) }
    (document.getElementById("button") as HTMLSpanElement).onmousedown = { buttonMouseDown() }
    (document.getElementById("button") as HTMLSpanElement).asDynamic().ontouchstart = { buttonClickOrDismiss(); pointerDown() }
    (document.getElementById("won-container") as HTMLDivElement).onclick = { buttonClickOrDismiss() }
    document.onkeydown = { keyDown(it) }
}

fun nameKeyUp(event: KeyboardEvent) {
    if (event.keyCode == 13) {
        event.preventDefault()
        event.stopPropagation()
        nameDone()
    }
}

fun roomKeyUp(event: KeyboardEvent) {
    if (event.keyCode == 13) {
        event.preventDefault()
        event.stopPropagation()
        roomDone()
    }
}

var ignoreNextClick = false
fun buttonMouseDown() {
    if (ignoreNextClick) {
        ignoreNextClick = false
        return
    }
    buttonClickOrDismiss()
}
fun buttonClickOrDismiss() {
    when(winner) {
        true -> dismissWon()
        null -> buttonClick()
    }
}
fun pointerDown() {
    ignoreNextClick = true
}

fun buttonClick() {
    winner = null
    send(byteArrayOf(-1))
}

fun dismissWon() {
    if (ignoreNextClick) {
        ignoreNextClick = false
        return
    }
    winner = null
    switchPage("button-container")
    send(byteArrayOf(-2))
}

var gameVisible = false
fun keyDown(event: KeyboardEvent) {
    if ((event.keyCode == 13 || event.keyCode == 32) && gameVisible && !inChat) {
        event.preventDefault()
        event.stopPropagation()
        buttonClickOrDismiss()
    }
}

var inChat = false
fun enableChat() {
    switchPage("chat-container")
    inChat = true
}
fun disableChat() {
    switchPage("button-container")
    inChat = false
}
fun chatKeyUp(event: KeyboardEvent) {
    if (event.keyCode == 13) {
        event.preventDefault()
        event.stopPropagation()
        sendChat()
    }
}
fun sendChat() {
    val msg = (document.getElementById("message-input") as HTMLInputElement).value
    if (msg.isNotBlank())
        send(msg)
    (document.getElementById("message-input") as HTMLInputElement).value = ""
}