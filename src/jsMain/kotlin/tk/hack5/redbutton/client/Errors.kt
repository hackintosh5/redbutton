/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.client

import kotlin.browser.document

fun showError(vararg e: Any?) {
    if (e.isNotEmpty() && e[0] is HiddenException)
        return
    console.error(e)
    document.getElementById("loading-title")!!.textContent = "Loading Failed"
    document.getElementById("loading-tip")!!.textContent = "Debugging information:\n${e.joinToString("\n")}\n${Error("Stack:").asDynamic().stack}"
    throw HiddenException()
}

class HiddenException : Exception()