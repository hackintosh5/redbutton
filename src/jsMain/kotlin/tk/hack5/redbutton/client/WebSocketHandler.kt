/*
 *     RedButton (university challenge webapp)
 *     Copyright (C) 2020 Hackintosh5
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tk.hack5.redbutton.client

import kotlinx.coroutines.Job
import org.khronos.webgl.Int8Array
import org.w3c.dom.MessageEvent
import org.w3c.dom.WebSocket
import org.w3c.dom.get
import org.w3c.files.Blob
import kotlin.browser.document
import kotlin.browser.window
import kotlin.dom.createElement

fun handleWebSocketMessage(e: MessageEvent) {
    when (e.data) {
        is Blob -> handleRttMessage(e.data.unsafeCast<Blob>())
        is String -> handleTextMessage(e.data.unsafeCast<String>())
    }
}

fun handleRttMessage(blob: Blob) {
    send(blob)
}

var winner: Boolean? = null

fun addMessage(msg: String, inChat: Boolean) {
    val elem = document.createElement("p") {
        textContent = msg
        classList.add("chat-message")
    }
    val messagesBox = if (inChat)
        document.getElementById("chat-messages-box")!!
    else
        document.getElementById("button-messages-box")!!
    if (inChat) {
        //messagesBox.appendChild(elem)
        messagesBox.insertBefore(elem, messagesBox.children[messagesBox.childElementCount - 1])
    } else {
        messagesBox.insertBefore(elem, messagesBox.firstElementChild)
        //messagesBox.appendChild(elem)
    }
}

fun handleTextMessage(string: String) {
    when(string[0]) {
        'm' -> {
            val msg = string.substring(1)
            addMessage(msg, true)
            addMessage(msg, false)
        }
        'l' -> {
            if (inChat) return
            document.getElementById("lost-text")!!.textContent = "Shh, it's ${string.substring(1)}'s turn"
            switchPage("lost-container")
            winner = false
        }
        'w' -> {
            if (inChat) return
            switchPage("won-container")
            winner = true
        }
        'd' -> {
            if (inChat) return
            switchPage("button-container")
            winner = null
        }
    }
}

fun send(message: String) = webSocketWrite!!(message)
fun send(message: ByteArray) = webSocketWriteBinary!!(message)
fun send(message: Blob) = webSocketWriteBlob!!(message)

fun connectionLost() {
    switchPage("loading-container")
    showError("Connection Terminated")
    webSocketWrite = null
}

var webSocketWrite: ((String) -> Unit)? = null
var webSocketWriteBinary: ((ByteArray) -> Unit)? = null
var webSocketWriteBlob: ((Blob) -> Unit)? = null


suspend fun connectToServer() {
    if (gameId.isEmpty()) {
        promptGameId()
        roomDoneWait.join()
    }
    // TODO https://github.com/ktorio/ktor/issues/1400
    val protocol = if (window.location.protocol == "http:") "ws" else "wss"
    val ws = WebSocket("$protocol://${window.location.host}/ws/$gameId")
    val open = Job()
    ws.onopen = {
        if (ws.readyState == 1.toShort()) {
            webSocketWrite = ws::send
            webSocketWriteBinary = {
                ws.send(Int8Array(it.toTypedArray()))
            }
            webSocketWriteBlob = ws::send
            open.complete()
        }
    }
    ws.onerror = {
        showError(it)
        open.completeExceptionally(RuntimeException(it.toString()))
    }
    ws.onclose = { connectionLost() }
    ws.onmessage = { handleWebSocketMessage(it) }
    window.setInterval({
        window.fetch("${window.location.protocol}//${window.location.host}/ping")
    }, 60000)
    open.join()
}